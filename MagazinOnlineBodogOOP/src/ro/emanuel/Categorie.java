package ro.emanuel;

import java.util.ArrayList;

public class Categorie {
	private ArrayList<Produs> produse;
	private String nume;

	public Categorie() {

	}

	public Categorie(String nume) {
		this.nume = nume;
		produse = new ArrayList<Produs>();
	}

	public void adaugaProdus(Produs p) {
		this.produse.add(p);
	}

	public void stergeProdus(Produs p) {
		this.produse.remove(p);
	}

	public void afisareProduse() {
		System.out.println("Afisare produse categoria : " + this.nume);
		for (Produs p : this.produse) {
			System.out.println(
					"Denumire : " + p.getDenumire() + " , Pret : " + p.getPret() + ", Cantitate : " + p.getCantitate());
		}
	}

	public ArrayList<Produs> getProduse() {
		return produse;
	}

	public void setProduse(ArrayList<Produs> produse) {
		this.produse = produse;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

}
