package ro.emanuel;

import java.util.ArrayList;

public class Magazin {
	private ArrayList<Categorie> categorii;
	private ArrayList<User> useri ;
	private String nume;

	public Magazin() {

	}

	public Magazin(String nume) {
		this.nume = nume;
		categorii = new ArrayList<Categorie>();
		useri = new ArrayList<User>();
	}

	public void adaugaUser(User u) {
		useri.add(u);
	}
	public void stergeUser(User u ) {
		this.useri.remove(u);
	}
	public ArrayList<Categorie> getCategorii() {
		return categorii;
	}

	public void setCategorii(ArrayList<Categorie> categorii) {
		this.categorii = categorii;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

}
