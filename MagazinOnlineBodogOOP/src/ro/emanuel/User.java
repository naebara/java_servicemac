package ro.emanuel;

import java.util.ArrayList;

public class User {
	
	private CosCumparaturi cos ;
	private ArrayList<Comanda> comenzi ;
	private String username ;
	
	
	
	public User(String username) {
		this.username = username;
		this.cos = new CosCumparaturi();
		this.comenzi = new ArrayList<Comanda>();
	}
	public User() {
		
	}
	
	public String toString() {
		return "Nume : " + this.username  ;
	}
	
	public void adaugaProdus(Produs p) {
		
		this.cos.adaugaProdus(p);
		
	}

	public void afisareComenzi() {
		for(Comanda c : this.comenzi) {
			System.out.println(c);
		}
	}
	
	public void stergeProdus(Produs p) {
		this.cos.stergeProdus(p);
	}
	
	public void afisareCos() {
		this.cos.afisareContinut();
	}
	
	public void checkOut() {

		Comanda a = new Comanda();
		a.setProduse(cos.getProduse());
		
		a.setTotal(this.cos.getTotal());
		comenzi.add(a);
		//this.cos = new CosCumparaturi();
		this.cos.setProduse(new ArrayList<Produs>());
	}
	public CosCumparaturi getCos() {
		return cos;
	}
	public void setCos(CosCumparaturi cos) {
		this.cos = cos;
	}
	public ArrayList<Comanda> getComenzi() {
		return comenzi;
	}
	public void setComenzi(ArrayList<Comanda> comenzi) {
		this.comenzi = comenzi;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	
}
