package ro.emanuel;

public class BMW extends Car {
	
	public void startEngine() {
		System.out.println("BMW - start engine");
		this.setOn(true);
		this.setLightsOn(true);
	}
	public void stopEngine() {
		System.out.println("bmw - stop engine");
		this.setLightsOn(false);
		this.setOn(false);
	}
}
