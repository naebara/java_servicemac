package ro.emanuel;

public class Logan extends Dacia {
	
	private boolean isTurobo;

	public void startEngine() {
		System.out.println("Logan - start engine");
		this.setLightsOn(true);
		this.isTurobo = true ;
	}
	
	public boolean isTurobo() {
		return isTurobo;
	}

	public void setTurobo(boolean isTurobo) {
		this.isTurobo = isTurobo;
	}

}
