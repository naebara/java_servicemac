package ro.emanuel;

public class Dacia extends Car {
	public Dacia() {

	}
	
	public void startEngine() {
		System.out.println("Dacia -START ENGINE");
		super.setOn(true);
	}
	
	public void stopEngine() {
		System.out.println("Dacia - Stop engine");
		this.setOn(false); 
		
	}
	
}
