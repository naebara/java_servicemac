package ro.emanuel;

public class Cladire extends Proprietate {

	public Cladire(Adresa adresa, int suprafata) {
		super(adresa, suprafata);
	}
	
	public int calculImpozit() {
		return 500 * this.getSuprafata();
	}
	public String toString() {
		return "\n\tCladire: " + this.getAdresa() + "\n\t\t Suprafata: " + this.getSuprafata() + "\n\t\t Cost: " + this.calculImpozit() + "\n" ;
	}
}
