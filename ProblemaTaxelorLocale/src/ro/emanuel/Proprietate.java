package ro.emanuel;

public abstract class Proprietate {
	
	private Adresa adresa ;
	private int suprafata ;
	public Proprietate(Adresa adresa , int suprafata) {
		this.adresa = adresa ;
		this.suprafata = suprafata ;
	}
	
	
	public abstract int calculImpozit();

	public Adresa getAdresa() {
		return adresa;
	}

	public void setAdresa(Adresa adresa) {
		this.adresa = adresa;
	}

	public int getSuprafata() {
		return suprafata;
	}

	public void setSuprafata(int suprafata) {
		this.suprafata = suprafata;
	}
	
	
}	
