package ro.emanuel;

public class Teren extends Proprietate {
	
	private int rang = 1 ;
	
	public Teren(Adresa adresa , int suprafata, int rang) {
		super(adresa , suprafata);
		this.rang = rang ;
	}
	public String toString() {
		return "\n\tTeren : " + this.getAdresa() + "\n\t\tSuprafata: " + this.getSuprafata() + "\n\t\tRang: " + this.getRang()  + "\n\t\tCost: " + this.calculImpozit() + "\n" ;
	}
	public int  calculImpozit() {
		return (350 * this.getSuprafata() ) / this.rang;
	}

	public int getRang() {
		return rang;
	}

	public void setRang(int rang) {
		this.rang = rang;
	}
	
	
}
