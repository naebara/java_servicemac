package ro.emanuel;

import java.util.ArrayList;

public class Contribuabil {

	private String nume ;
	private String CNP ;
	private ArrayList<Proprietate> proprietati ;
	
	public Contribuabil(String nume , String CNP) {
		this.nume = nume ;
		this.CNP = CNP ;
		proprietati = new ArrayList<Proprietate>();
	}
	
	public void adaugaProprietate(Proprietate p ) {
		this.proprietati.add(p);
	}
	public void stergeProprietate(Proprietate p ) {
		this.proprietati.remove(p);
	}
	
	public String toString() {
		String prop = "" ;
		for(Proprietate p : proprietati) {
			prop += p ;
		}
		return "Contribuabil : " +  this.nume + ", CNP: " + this.CNP + "\nProprietati" + prop + "\nTotal: "+ calculImpozit();
	}
	
	public int calculImpozit() {
		int impozitTotal = 0 ;
		for(Proprietate p : proprietati) {
			impozitTotal += p.calculImpozit() ;
		}
		return impozitTotal ;
	}
	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getCNP() {
		return CNP;
	}

	public void setCNP(String cNP) {
		CNP = cNP;
	}

	public ArrayList<Proprietate> getProprietati() {
		return proprietati;
	}

	public void setProprietati(ArrayList<Proprietate> proprietati) {
		this.proprietati = proprietati;
	}
	
	
}
