package ro.emanuel;

import java.util.ArrayList;

public class Registru {
	private ArrayList<Contribuabil> contribuabili ;
	
	public Registru() {
		contribuabili = new ArrayList<Contribuabil>();
	}

	public ArrayList<Contribuabil> getContribuabili() {
		return contribuabili;
	}

	public void setContribuabili(ArrayList<Contribuabil> contribuabili) {
		this.contribuabili = contribuabili;
	}



	
}
