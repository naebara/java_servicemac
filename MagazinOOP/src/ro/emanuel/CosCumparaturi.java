package ro.emanuel;

import java.util.ArrayList;

public class CosCumparaturi{
	private ArrayList<Produs> produse ;
	
	public CosCumparaturi() {
		this.produse = new ArrayList<Produs>();
	}
	public void afisareCos() {
		for (int i = 0; i < produse.size(); i++) {
			System.out.println("denumire : " + produse.get(i).getDenumireProdus() + ", pret : " + produse.get(i).getPret() + ", cantitate : " + produse.get(i).getCantitate());
		}
	}
	
	public ArrayList<Produs> getProduse() {
		return produse;
	}
	public void setProduse(ArrayList<Produs> produse) {
		this.produse = produse;
	}
	public String toString() {
		var result = "" ;
		for (int i = 0; i < produse.size(); i++) {
			if(produse.get(i).getCantitate() > 0 )
			result += produse.get(i).getDenumireProdus() + ", pret : " + produse.get(i).getPret() +  ", cantitate: " + produse.get(i).getCantitate() + " \n" ;
		}
		return result ;
	}
}
