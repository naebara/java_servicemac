package ro.emanuel;

public class User {
	private CosCumparaturi cos ;
	private String nume ;
	private int pretPerTotal = 0 ;
	public User(String nume ) {
		this.nume = nume ;
		this.cos = new CosCumparaturi(); ;
	}
	public void adaugareProdus(Produs p) {
		this.cos.getProduse().add(p);
		p.setStoc(p.getStoc() - p.getCantitate());
		this.pretPerTotal += p.getPret()*p.getCantitate();
	}
	public void stergereProdus(Produs p ) {
		int ok = 1 ;
		for (int i = 0; i < cos.getProduse().size() && ok == 1; i++) {
			if(p.getDenumireProdus() == this.cos.getProduse().get(i).getDenumireProdus()) {
				ok = 0 ; 
			
				this.cos.getProduse().get(i).setCantitate(p.getCantitate() -1 );
			}
		}
		p.setStoc(p.getStoc()+ p.getCantitate());
		this.pretPerTotal -= p.getPret();
	}
	
	public String toString() {
		return "User: " + this.nume + "\ncos:\n" + cos  + "Total: " + this.pretPerTotal ;
	}
	public void checkOut() {
		this.cos = new CosCumparaturi();
		this.pretPerTotal = 0 ;
	}
	public CosCumparaturi getCos() {
		return cos;
	}
	public void setCos(CosCumparaturi cos) {
		this.cos = cos;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	
}
