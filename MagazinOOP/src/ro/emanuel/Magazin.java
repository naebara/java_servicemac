package ro.emanuel;

import java.util.ArrayList;

public class Magazin {
	private ArrayList<Produs> produseMagazin;
	private ArrayList<User> users;

	public Magazin() {
		this.produseMagazin = new ArrayList<Produs>();
		this.users = new ArrayList<User>();
	}

	public ArrayList<Produs> getProduseMagazin() {
		return produseMagazin;
	}

	public void setProduseMagazin(ArrayList<Produs> produseMagazin) {
		this.produseMagazin = produseMagazin;
	}

	public ArrayList<User> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<User> users) {
		this.users = users;
	}

	public String toString() {
		var result = "";
		for (int i = 0; i < users.size(); i++) {
			result += users.get(i).getNume() + "\n";
		}
		return result ;
	}
	
}
