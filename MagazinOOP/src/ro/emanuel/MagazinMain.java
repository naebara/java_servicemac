package ro.emanuel;

public class MagazinMain {
	public static void main(String[] args) {
		Magazin maga = new Magazin();
		User nae = new User("Nae");
		User dani = new User("Dani");
		
		maga.getUsers().add(nae);
		maga.getUsers().add(dani);
		System.out.println("am creat magazin si am adaugat 2 clienti");
		Produs p1 = new Produs("aliment", 23, 1, "sare");
		Produs p2 = new Produs("electronic", 2300, 3, "laptop");
		Produs p3 = new Produs("igiena", 10, 1, "perie");
		Produs p4 = new Produs("aliment", 5, 1, "piper");
		
		nae.adaugareProdus(p1);
		nae.adaugareProdus(p2);
		System.out.println("client nae dupa adaugare produse in cos");
		System.out.println(nae);
		System.out.println("stergere prosude nae");
		nae.stergereProdus(p2);
		nae.adaugareProdus(p1);
		System.out.println("client nae dupa stergere produse");
		System.out.println(nae);
		

	}
}
