package ro.emanuel;
// categorii : alimente , igiena si electronice 
public class Produs extends CategoriiProduse {
	
	private int pret , cantitate  ;
	private int stoc = 100 ;
	private String denumireProdus ;
	public Produs(String categorie, int pret, int cantitate, String denumireProdus) {
		super(categorie);
		this.pret = pret;
		this.cantitate = cantitate;
		this.denumireProdus = denumireProdus;
	}
	
	public int getStoc() {
		return stoc ;
	}
	
	public void setStoc(int stoc) {
		this.stoc = stoc ;
	}
	
	public int getPret() {
		return pret;
	}
	public void setPret(int pret) {
		this.pret = pret;
	}
	public int getCantitate() {
		return cantitate;
	}
	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}
	public String getDenumireProdus() {
		return denumireProdus;
	}
	public void setDenumireProdus(String denumireProdus) {
		this.denumireProdus = denumireProdus;
	}
	
}
