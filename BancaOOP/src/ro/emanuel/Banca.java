package ro.emanuel;



import java.util.ArrayList;

public class Banca {
	private ArrayList<Client> clienti;
	private String numeBanca;

	public Banca(String numeBanca) {
		clienti = new ArrayList<Client>();
		this.numeBanca = numeBanca;
	}
	
	public void addClient(Client a) {
		clienti.add(a);
	}

	public void stergeClient(Client a) {
		for (int i = 0; i < clienti.size(); i++) {
			if (clienti.get(i).getNume() == a.getNume() && clienti.get(i).getPrenume() == a.getPrenume())
					{clienti.get(i).setNume("");
					clienti.get(i).setPrenume("");return;
					}
					
		}
	}

	public String getSold() {
		int sR = 0;
		int sE = 0;
		String result = "" ;
		for (int i = 0; i < clienti.size(); i++) {
			for (int k = 0; k < clienti.get(i).getConturi().size(); k++) {
				if (clienti.get(i).getConturi().get(k).getMoneda() == "RON")
					sR += clienti.get(i).getConturi().get(k).getSold();
			}

		}
		for (int i = 0; i < clienti.size(); i++) {
			for (int k = 0; k < clienti.get(i).getConturi().size(); k++) {
				if (clienti.get(i).getConturi().get(k).getMoneda() == "EUR")
					sE += clienti.get(i).getConturi().get(k).getSold();
			}

		}
		return "BANCA: " +  numeBanca  +  " \nSold Ron : " + sR + "\nSold Eur : " + sE ;
	}

	public String toString() {
		String clie = "BANCA " + numeBanca + "::CLIENTI\n";
		for (int i = 0; i < clienti.size(); i++) {
			if(clienti.get(i).getNume() !="")
				clie += clienti.get(i).getNume() + " " + clienti.get(i).getPrenume() + " \n";
		}
		return clie;
	}


}
