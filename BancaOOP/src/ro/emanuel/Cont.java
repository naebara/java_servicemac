package ro.emanuel;

public class Cont {
	private String moneda;
	private int sold  ;
	
	public Cont(String moneda) {
		this.sold = 0 ;
		this.moneda = moneda;
	}
	
	public void setSold(int sold) {
		this.sold += sold;
	}
	
	public int getSold() {
		return this.sold;
	}
	public void depunereBani(int suma) {
		this.setSold(suma);
	}
	public String getMoneda() {
		return moneda;
	}

	public String toString() {
		return "Cont [moneda=" + moneda + ", sold=" + sold + "]";
	}
	
}
