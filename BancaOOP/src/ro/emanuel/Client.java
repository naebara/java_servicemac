package ro.emanuel;

import java.util.ArrayList;

public class Client {
	private ArrayList<Cont> conturi ;
	private String nume , prenume ;
	public Client(String nume, String prenume) {
		this.nume = nume ;
		this.prenume = prenume ;
		this.conturi = new ArrayList<Cont>();
	}
	// transfer
	
	public void transfer(String contMoneda,Client b , int suma) {
		for (int i = 0; i < b.getConturi().size(); i++) {
			if(contMoneda == b.getConturi().get(i).getMoneda())
				b.getConturi().get(i).setSold(suma);
		}
		for (int i = 0; i < conturi.size(); i++) {
			if(conturi.get(i).getMoneda() == contMoneda)
				conturi.get(i).setSold(-suma);
		}
		
	}
	public Cont contDupaNume(String moneda) {
		for (int i = 0; i < conturi.size(); i++) {
			if(conturi.get(i).getMoneda() == moneda)
			return conturi.get(i);
		}
		return null;
		
	}
	////////////////////////////////////////////////

	public void afisareSoldMoneda(String moneda) {
		for (int i = 0; i < conturi.size(); i++) {
			if(moneda == conturi.get(i).getMoneda())
				System.out.println("sold " + "moneda = " +conturi.get(i).getSold());
		}
	}
	public void afisareSoldClient() {
		for (int i = 0; i < conturi.size(); i++) {
			System.out.println("sold " + conturi.get(i).getMoneda() + " " +conturi.get(i).getSold()+"\n");
		}
	}
	
	
	public ArrayList<Cont> getConturi() {
		return conturi;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public String getPrenume() {
		return prenume;
	}
	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}

	@Override
	public String toString() {
		return nume + " " + prenume + ", conturi : " + conturi ;
	}
	
	
}
