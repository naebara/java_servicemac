package ro.emanuel;

public class MainBanca {
	public static void main(String[] args) {
		System.out.println(" nu am reusit transfer dintr-un curs la alt curs(modede diferite)\n" + 
				" nu am reusit transfer intre banci comision");
		System.out.println("am creat banca ing si adaug clienti");
		Banca ing = new Banca("ING");
		Client nae = new Client("Bara", "Nae");
		Client ionel = new Client("Gaidos", "Ionel");
		Client emil = new Client("Pit", "Emil");
		Client alex = new Client("Buzle", "Alex");
		Client elisa = new Client("Marin", "Elisa");
		Client miha = new Client("Mihut", "Miha");
		ing.addClient(nae);
		ing.addClient(emil);
		ing.addClient(elisa);
		ing.addClient(miha);
		ing.addClient(alex);
		ing.addClient(ionel);
		nae.getConturi().add(new Cont("EUR"));
		nae.getConturi().add(new Cont("RON"));
		ionel.getConturi().add(new Cont("EUR"));
		ionel.getConturi().add(new Cont("RON"));
		nae.contDupaNume("EUR").depunereBani(400);
		nae.contDupaNume("EUR").depunereBani(500);
		nae.contDupaNume("RON").depunereBani(200);
		nae.contDupaNume("EUR").depunereBani(400);
		ionel.contDupaNume("RON").depunereBani(100);
		ionel.contDupaNume("RON").depunereBani(400);
		ionel.contDupaNume("EUR").depunereBani(700);
		System.out.println("\ntransfer de la nae la ionel 50 ron\n");
		System.out.println(ionel);
		System.out.println(nae);
		nae.transfer("RON", ionel, 50);
		System.out.println(ionel);
		System.out.println(nae);
		System.out.println("\nstergere client miha\n");
		System.out.println(ing);
		ing.stergeClient(miha);
		System.out.println(ing);
		System.out.println("client miha sters - nu se vede in lista--corect\n");
		System.out.println("\nafisare sold ING");
		System.out.println(ing.getSold());
		/// new bank
		
		System.out.println("\nam creat alta banac bnr si adaug clienti\n");
		Banca bnr = new Banca("BNR");
		Client marin = new Client("Maghiar", "Marin");
		Client sergiu = new Client("Podila", "Sergiu");
		Client dorin = new Client("Cuc", "Dorin");
		Client matei = new Client("Copaciu", "Matei");
		Client andreea = new Client("Anca", "Andreea");
		Client florina = new Client("Duca", "Florina");
		bnr.addClient(marin);
		bnr.addClient(dorin);
		bnr.addClient(andreea);
		bnr.addClient(florina);
		bnr.addClient(matei);
		bnr.addClient(sergiu);
		marin.getConturi().add(new Cont("EUR"));
		marin.getConturi().add(new Cont("RON"));
		sergiu.getConturi().add(new Cont("EUR"));
		sergiu.getConturi().add(new Cont("RON"));
		marin.contDupaNume("EUR").depunereBani(4300);
		marin.contDupaNume("EUR").depunereBani(5200);
		marin.contDupaNume("RON").depunereBani(2200);
		marin.contDupaNume("EUR").depunereBani(4400);
		sergiu.contDupaNume("RON").depunereBani(5100);
		sergiu.contDupaNume("RON").depunereBani(4060);
		sergiu.contDupaNume("EUR").depunereBani(7040);
		System.out.println("\ntransfer de la marin la sergiu 50 eur\n");
		System.out.println(sergiu);
		System.out.println(marin);
		marin.transfer("EUR", sergiu, 50);
		System.out.println(sergiu);
		System.out.println(marin);
		System.out.println("\ntransfer/plata efectuata\n");
		System.out.println("\nstergere client florina\n");
		System.out.println(bnr);
		bnr.stergeClient(florina);
		System.out.println(bnr);
		System.out.println("\nclient florina sters\n");
		System.out.println("\nafisare sold BNR");
		System.out.println(bnr.getSold());
		
		System.out.println("\n\ntransfer intre clienti de la banci diferite");
		System.out.println("nae este client ING");
		System.out.println("sergiu este client BNR");
		System.out.println("se va transfera de la sergiu la nae 6000 euro\n");
		System.out.println(nae);
		System.out.println(sergiu);
		sergiu.transfer("EUR", nae, 6000);
		System.out.println(nae);
		System.out.println(sergiu);
		System.out.println("\ntransfer/plata efectuata");
	}
}
