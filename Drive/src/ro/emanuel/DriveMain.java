package ro.emanuel;

public class DriveMain {
	
	public static void main(String[] args) {
		Folder a = new Drive("Hello");
		Drive doc = new Drive("Documents");
		Folder natanael = new Folder("Natanael");
		Folder teme = new Folder("Teme");
		Folder muzica = new Folder("Muzica");
		Folder poze = new Folder("Poze");
		File tema1 = new File("Tema1", "txt", "de predat maine ");
		File tema2 = new File("Tema2", "txt", "mihaela are teme multe");
		File tema3 = new File("Tema3", "txt", "alex are o masina alba ");
		File tema4 = new File("Tema4", "txt", "iarna pe ulita");
		File file1 = new File("File1", "txt", "this is english test");
		File file2 = new File("File2", "txt", "paper to be submitted");
		File file3 = new File("File3", "txt", "He is the CEO of Apple Company");
		doc.adaugaFolder(teme);
		doc.adaugaFolder(muzica);
		doc.adaugaFolder(poze);
		doc.adaugaFolder(natanael);
		doc.adaugaFisier(file1);
		doc.adaugaFisier(file2);
		doc.adaugaFisier(file3);
		teme.adaugaFisier(tema1);
		teme.adaugaFisier(tema2);
		teme.adaugaFisier(tema3);
		teme.adaugaFisier(tema4);
		
		// operatii
		System.out.println("am creat un drive si am adaugat foldere si fisiere");
		System.out.println("\nAfisare folder Documents\n");
		System.out.println(doc);
		
		System.out.println("\nFolderul teme cu continutul lui\n");
		System.out.println(teme);
		
		System.out.println("\nAfisare continut fisiere din folder Teme\n");
		teme.arataContentFisiereFolder();
		System.out.println("\nAfisare continut fisiere din folder Documents\n");
		doc.arataContentFisiereFolder();
		System.out.println("Se va cauta foldere dupa un substring pe care il voi da" +
		"\n,am folderul 'Natanael' si voi cauta dupa substring-ul 'tan' \n"
				);
		System.out.println("--------");
		doc.searchByPartialSubString("tan");
		System.out.println("--------");
		
		System.out.println("\nStergere folder din Documents(merge de oriunde)\n\nDocuments inainte de stergere fisier 2\n");
		System.out.println(doc);
		doc.stergeFisier(file2);
		System.out.println(doc);
		System.out.println("Documents dupa stergere fisier 2");
		
	}
	
	
	
}
