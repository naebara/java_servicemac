package ro.emanuel;

import java.util.ArrayList;

public class Folder {
	private String nume ;
	private ArrayList<Folder> foldere;
	private ArrayList<File> fisiere;
	
	public Folder(String nume) {
		this.nume = nume ;
		this.foldere = new ArrayList<Folder>();
		this.fisiere = new ArrayList<File>();
	}
	
	public void adaugaFolder(Folder a) {
		foldere.add(a);
	}
	
	public void adaugaFisier(File a) {
		fisiere.add(a);
	}
	
	public void arataContentFisiereFolder() {
		String result = "" ;
			for (int i = 0; i < this.getFisiere().size(); i++) {
				System.out.println( "continut fisier -> " + this.getFisiere().get(i).getNume() + "." +  this.getFisiere().get(i).getExtensie() + " :");
				System.out.println( this.getFisiere().get(i).getContinut()  );
			}
	}
	
	public void searchByPartialSubString(String partial) {
	
		for (int i = 0; i < fisiere.size(); i++) {
			if(fisiere.get(i).getContinut().toLowerCase().contains(partial.toLowerCase()))
				System.out.println(fisiere.get(i).getNume());
		}
	}
	
	public void stergeFisier( File a  ) {
		for (int i = 0; i < fisiere.size() ; i++) {
			if(a.getNume() == fisiere.get(i).getNume()) {
				for (int j = i+1; j < fisiere.size(); j++) {
					fisiere.get(i).setNume("");
					fisiere.get(i).setExtensie("");
					fisiere.get(i).setContinut("");
				}
			}
		}
		
	}
	
	public String toString() {
		String result = "Folder : " + this.getNume() ;
		result += "\nFoldere : \n" ;
		for (int i = 0; i < foldere.size(); i++) {
			if(foldere.get(i).getNume() !="")
			result += "->" +foldere.get(i).getNume() + "\n";
		}
		result += "\nFisiere : \n";
		for (int i = 0; i < fisiere.size(); i++) {
			if(fisiere.get(i).getNume() !="")
			result += "->" +fisiere.get(i).getNume() + "\n";
		}
		return result ;
	}
	
	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public ArrayList<Folder> getFoldere() {
		return foldere;
	}

	public void setFoldere(ArrayList<Folder> foldere) {
		this.foldere = foldere;
	}

	public ArrayList<File> getFisiere() {
		return fisiere;
	}

	public void setFisiere(ArrayList<File> fisiere) {
		this.fisiere = fisiere;
	}
	
	
}
