package ro.emanuel;

public class File {
	private String nume , extensie, continut ;
	
	public File(String nume, String extensie , String continut) {
		this.nume = nume ;
		this.extensie = extensie ;
		this.continut = continut ;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getExtensie() {
		return extensie;
	}

	public void setExtensie(String extensie) {
		this.extensie = extensie;
	}

	public String getContinut() {
		return continut;
	}

	public void setContinut(String continut) {
		this.continut = continut;
	}
	
}
