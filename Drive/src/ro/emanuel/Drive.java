package ro.emanuel;

import java.util.ArrayList;

public class Drive extends Folder {

	public Drive(String nume) {
		super(nume);
	}
	
	public String toString() {
		String result = "Drive : " + this.getNume();
		result += "\nFoldere : \n" ;
		for (int i = 0; i < this.getFoldere().size(); i++) {
			if(this.getFoldere().get(i).getNume()!="")
			result += "->" + this.getFoldere().get(i).getNume() + "\n";
		}
		result += "\nFisiere : \n";
		for (int i = 0; i < this.getFisiere().size(); i++) {
			if(this.getFisiere().get(i).getNume()!="")
			result += "->" + this.getFisiere().get(i).getNume() + "\n";
		}
		return result ;
	}
	
}
