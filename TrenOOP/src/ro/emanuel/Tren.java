package ro.emanuel;

import java.util.ArrayList;

public class Tren {
	private String nume ;
	private int capacitate;
	private ArrayList<String> pasageri;

	public Tren(String nume, int capacitate) {
		this.nume = nume;
		this.capacitate = capacitate;
		this.pasageri = new ArrayList<String>();
	}
	
	public void urcarePasageri(ArrayList<String> pasageri) {
		
		System.out.println("--Urcare Pasageri--Tren stop--");
		
			if(this.pasageri.size() + pasageri.size() > this.capacitate) {
				System.out.println("--Capacitate maxima atinsa--"
					+ "\n--Nu puteti intra--");
				return;
				}
			
			for(String a : pasageri) {
			this.pasageri.add(a);
		}
	}
	
	public void coborarePasageri(ArrayList<String> pasageri) {
		System.out.println("--Coborare pasageri--Tren stop--");
		
		for(String a : pasageri) {
			this.pasageri.remove(a);
		}
	}
	
	public String toString() {
		String result = "--Pasagerii ramasi in tren--  \n\n" ;
		for(String a : pasageri) {
			result += "  ->  " + a + "\n";
		}
		return result ;
	}
	
	// la aceasta tema nu m-am folosit de gettere si settere 
	// deci nu le-am creat pentru inutilitate :) ... 
}
