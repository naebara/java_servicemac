package ro.emanuel;

import java.util.ArrayList;
import java.util.Arrays;

public class TrenMain {
	
	public static void main(String[] args) {
		
		
		Tren t = new Tren("H384" , 10);
		System.out.println(t);
		ArrayList<String> pasageriNoi = new ArrayList<String>(Arrays.asList("Marius", "Andrei", "Evelina"));
		t.urcarePasageri(pasageriNoi);
		pasageriNoi = new ArrayList<String>(Arrays.asList("Ionel", "Marcel", "Nae"));
		t.urcarePasageri(pasageriNoi);
		System.out.println(t);
		ArrayList<String> pasageriOut = new ArrayList<String>(Arrays.asList("Marius", "Evelina"));
		t.coborarePasageri(pasageriOut);
		System.out.println(t);
		pasageriNoi = new ArrayList<String>(Arrays.asList("John", "Nicolae", "Messi"));
		t.urcarePasageri(pasageriNoi);
		System.out.println(t);
		pasageriOut = new ArrayList<String>(Arrays.asList("John", "Messi"));
		t.coborarePasageri(pasageriOut);
		System.out.println(t);
		System.out.println("Aici incerc sa adaug pasageri peste capacitatea maxima.\n");
		pasageriNoi = new ArrayList<String>(Arrays.asList("John", "Messi","John", "Messi","John", "Messi","John", "Messi","John", "Messi","John", "Messi"));
		t.urcarePasageri(pasageriNoi);
		System.out.println(t);
	}
	
}
