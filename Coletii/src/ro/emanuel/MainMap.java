package ro.emanuel;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;


public class MainMap {

	public static void main(String[] args) {
		// implementati dictionarul limbii romane
		
		HashMap<String, String> dex = new HashMap<String, String>();
		dex.put("java", "limbaj de programare de nivel inalt");
		dex.put("student" , "fat frumos/ileana cosanzeana in devenire");
		dex.put("avar", " definitie prea complicata");
		dex.put("cioco", " definitie prea complicata");
		
		// parcurgere chei si afisare values
		
		for(String key : dex.keySet()) {
			System.out.println(key + " : " + dex.get(key));
		}
		dex.remove("java");
		Set<Entry<String,String>> valori = dex.entrySet();
		for(Entry<String, String> e : valori) {
			System.out.println(e.getKey() + " : " + e.getValue());
		}
		
	}

}
