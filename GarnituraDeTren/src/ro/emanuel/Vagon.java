package ro.emanuel;

public abstract class Vagon {
	private int colete ;
	private int maxColete ;
	
	
	
	public int getColete() {
		return colete;
	}
	public void setColete(int colete) {
		this.colete = colete;
	}
	public int getMaxColete() {
		return maxColete;
	}
	public void setMaxColete(int maxColete) {
		this.maxColete = maxColete;
	}
	
	
}
