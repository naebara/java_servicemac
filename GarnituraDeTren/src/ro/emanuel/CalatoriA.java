package ro.emanuel;

public class CalatoriA extends Calatori{
	private final static int MAX_PASAGERI = 40 ;
	private final static int MAX_COLETE = 300 ;
	
	public void addPassenger() {
		if(this.getPasageri() < MAX_PASAGERI) {
			this.setPasageri(this.getPasageri() + 1 );
		}
	}
	public void removePassenger() {
		this.setPasageri(this.getPasageri() - 1 );
	}
	public  int getMaxPasageri() {
		return MAX_PASAGERI;
	}
	public  int getMaxColete() {
		return MAX_COLETE;
	}
	public String toString() {
		return "Calatori A " + "\n\tCalatori : " + this.getPasageri() + "\n\tColete :   " + this.getColete()  ;
	}
}
