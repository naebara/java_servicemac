package ro.emanuel;

import java.util.ArrayList;

public class Tren {
	private ArrayList<Vagon> vagoane = new ArrayList<Vagon>();
	private int maxVagoane = 15 ;
	
	public ArrayList<Vagon> getVagoane() {
		return vagoane;
	}

	public void setVagoane(ArrayList<Vagon> vagoane) {
		this.vagoane = vagoane;
	}

	public void addVagon(Vagon... args ) {
		for(Vagon a : args) {
			vagoane.add(a);
		}
	}
	
	public void removeVagon(Vagon v) {
		vagoane.remove(v);
	}
	public String toString() {
		var res = "Vagoane : \n" ;
		for(Vagon v : vagoane) {
			res += v + "\n" ;
		}
		return res ;
	}
}
