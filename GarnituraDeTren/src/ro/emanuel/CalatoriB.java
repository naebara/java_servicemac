package ro.emanuel;

public class CalatoriB extends Calatori {
	private final static int MAX_PASAGERI = 50 ;
	private final static int MAX_COLETE = 400 ;
	public void addPassenger() {
		if(this.getPasageri() < MAX_PASAGERI) {
			this.setPasageri(this.getPasageri() + 1 );
		}
		else {
			System.out.println("Nr pasageri maxim atins...sorry");
		}
	}
	public void removePassenger() {
		this.setPasageri(this.getPasageri() - 1 );
	}
	
	public void addColet() {
		if(this.getColete() < MAX_COLETE) {
			this.setColete(this.getColete() + 1);
		}
		else {
			System.out.println("nu putem adauga coletul dvs.");
		}
	}
	
	public void removeColet() {
		this.setColete(this.getColete() - 1 );
	}
	public  int getMaxPasageri() {
		return MAX_PASAGERI;
	}
	public  int getMaxColete() {
		return MAX_COLETE;
	}
	public String toString() {
		return "Calatori B " + "\n\tCalatori : " + this.getPasageri() + "\n\tColete :   " + this.getColete()  ;
	}
	
}
