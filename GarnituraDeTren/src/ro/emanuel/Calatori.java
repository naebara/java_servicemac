package ro.emanuel;

public abstract  class Calatori  extends Vagon{
	private int pasageri = 0 , maxPasageri ;
	
	
	public abstract void  addPassenger();
	public abstract void  removePassenger();
	
	public abstract String toString();
	public void openDoors() {
		System.out.println("Usi deschise");
	}
	
	public void closeDoos() {
		System.out.println("Usi inchise");
	}

	public void addColet() {
		this.setColete(this.getColete() + 1 );
	}
	
	public void removeColet() {
		this.setColete(this.getColete() - 1 );
	}
	public int getPasageri() {
		return pasageri;
	}

	public void setPasageri(int pasageri) {
		this.pasageri = pasageri;
	}

	public int getMaxPasageri() {
		return maxPasageri;
	}

	public void setMaxPasageri(int maxPasageri) {
		this.maxPasageri = maxPasageri;
	}
	
	
}
