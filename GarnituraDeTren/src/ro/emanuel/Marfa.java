package ro.emanuel;

public class Marfa extends Vagon {
	private final static int MAX_COLETE = 400 ;
	
	public void addColet() {
		if(this.getColete() < MAX_COLETE) {
			this.setColete(this.getColete() + 1 );
		}
		else {
			System.out.println("nr maxim colete atins... sorry ");
		}
	}
	
	public void  removeColet() {
		this.setColete(this.getColete() - 1 );
	}

	public  int getMaxColete() {
		return MAX_COLETE;
	}
	public String toString() {
		return  "Marfa : " + "\n\tColete : " + this.getColete()  ;
	}
}
