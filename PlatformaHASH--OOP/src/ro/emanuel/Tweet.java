package ro.emanuel;


public class Tweet {
	private String content;

	public Tweet(String content) {
		this.content = content;
		
	}
	public String getContent() {
		return content;
	}
	
	public String toString() {
	
		return this.getContent() ;
	}
}
