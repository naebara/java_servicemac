package ro.emanuel;

import java.util.HashMap;

public class MainPlatformaHash {
	
	// Platforma nu va fi o clasa... va fi un HASHMAP
	
	
	
public static void main(String[] args) {
		
		HashMap<String, User> insta = new HashMap<String,User>();
		
		User nae = new User("Nae");
		User nae1 = new User("Nae");
		User dani = new User("Dani");
		Tweet t1 = new Tweet("E primul meu Tweet");
		Tweet t2 = new Tweet("Al tweet al meu vine acum pe platforma");
		nae.publishTweet(t1);
		nae.publishTweet(t2);
		insta.putIfAbsent("Nae", nae);
		insta.putIfAbsent("Dani", dani);
		insta.put("Nae", nae1);
		for(String key : insta.keySet()) {
			System.out.println(key + " : " + insta.get(key) );
		}
		Tweet t3 = new Tweet("Tweet din 2 ianuarie");
		Tweet t4 = new Tweet("De ce nu am inteles programul ? ");
		dani.publishTweet(t3);
		dani.publishTweet(t4);
		insta.get("Dani").afisareTweets();
		
		String cautare = "ianu";
		
		System.out.println("cautare dupa cuvantul : '" + cautare+"'");
		for(String key : insta.keySet()) {
			for(Tweet t : insta.get(key).getTweets()) {
				if(t.getContent().contains(cautare)) {
					System.out.println("tweet gasit: " + t);
				}
			}
		}
		System.out.println("\nPlatforma : " + insta);
	}
}
