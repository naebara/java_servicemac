package ro.emanuel;

import java.sql.Date;
import java.util.ArrayList;

public class Universitate {
	
	private ArrayList<Persoana> persoanaUniv = new ArrayList<Persoana>();
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Universitate emanuel = new Universitate();
		Persoana p = new Student("Bara", "Natanael", new Date(1999,2,13), 'm', 33, 2018, "IE"	, false);
		System.out.println(p);
		Act act = new Diploma(1, new Date(2021 , 06 , 12), 9.45, 2021);
		p.addNewAct(act);
		emanuel.persoanaUniv.add(p);
		System.out.println(emanuel);
	}

}
