package ro.emanuel;

import java.util.Date;

public class Student extends Persoana {
	private int nrMatrocol;
	private int anStart;
	private String specializare;
	private boolean bursier;
	
	public Student(String nume, String prenume, Date dataNasterii, char gen, int nrMatrocol, int anStart,
			String specializare, boolean bursier) {
		super(nume, prenume, dataNasterii, gen);
		this.nrMatrocol = nrMatrocol;
		this.anStart = anStart;
		this.specializare = specializare;
		this.bursier = bursier;
	}
	
	
	public Student(String nume, String prenume, Date dataNasterii, char gen) {
		super(nume, prenume, dataNasterii, gen);
	}
	
	public int getNrMatrocol() {
		return nrMatrocol;
	}


	public void setNrMatrocol(int nrMatrocol) {
		this.nrMatrocol = nrMatrocol;
	}


	public int getAnStart() {
		return anStart;
	}


	public void setAnStart(int anStart) {
		this.anStart = anStart;
	}


	public String getSpecializare() {
		return specializare;
	}


	public void setSpecializare(String specializare) {
		this.specializare = specializare;
	}


	public boolean isBursier() {
		return bursier;
	}


	public void setBursier(boolean bursier) {
		this.bursier = bursier;
	}


	


	

}
