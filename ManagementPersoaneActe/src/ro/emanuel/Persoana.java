package ro.emanuel;

import java.util.ArrayList;
import java.util.Date;

public class Persoana {
	private String nume , prenume ;
	private Date dataNasterii ;
	private char gen ;
	private ArrayList<Act> listaActelor;
	public Persoana(String nume, String prenume, Date dataNasterii, char gen) {
		super();
		this.nume = nume;
		this.prenume = prenume;
		this.dataNasterii = dataNasterii;
		this.gen = gen;
		listaActelor = new ArrayList<Act>();
	}
	
	public void addNewAct(Act a) {
		listaActelor.add(a);
	}
	
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public String getPrenume() {
		return prenume;
	}
	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}
	public Date getDataNasterii() {
		return dataNasterii;
	}
	public void setDataNasterii(Date dataNasterii) {
		this.dataNasterii = dataNasterii;
	}
	public char getGen() {
		return gen;
	}
	public void setGen(char gen) {
		this.gen = gen;
	}
	
	
}
