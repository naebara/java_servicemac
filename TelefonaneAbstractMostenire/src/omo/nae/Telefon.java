package omo.nae;

public  class Telefon {
	private String manufactor, os, model ;
	private int price ;
	public Telefon(String manufactor, String os, String model, int price) {
		super();
		this.manufactor = manufactor;
		this.os = os;
		this.model = model;
		this.price = price;
	}
	public String getManufactor() {
		return manufactor;
	}
	public void setManufactor(String manufactor) {
		this.manufactor = manufactor;
	}
	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	
	
}
