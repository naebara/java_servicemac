package ro.emanuel;

import java.util.ArrayList;

public class User {
	private String nume ;
	private ArrayList<Tweet> tweets ;
	
	public User(String nume) {
		this.nume = nume ;
		this.tweets= new ArrayList<Tweet>();
	} 
	
	public void publishTweet(Tweet a) {
		if(a.getContent().length() > 140)
			System.out.println("Lungime maxima depasita -- imposibil de postat!!!");
		else tweets.add(a);
	}
	public void afisareTweets() {
		System.out.println("Tweet-urile userului " + this.nume + ": \n") ;
		for (int i = 0; i < tweets.size(); i++) {
			int nr = i+1;
			System.out.println("Tweet " + nr  + " : "+ tweets.get(i).getContent());
		}
		System.out.println();
	}
	public String getNume() {
		return nume ;
	}
	public ArrayList<Tweet> getTweets () {
		return tweets ;
	}
}
