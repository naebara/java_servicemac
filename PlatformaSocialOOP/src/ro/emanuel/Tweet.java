package ro.emanuel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Tweet {
	private String content;
	private Date data ;
	public Tweet(String content) {
		this.content = content;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		data = date;
		
	}
	public String getContent() {
		return content;
	}
	
	public String toString() {
		String result ="";
		result += data ;
		return result ;
	}
	
	public Date getData() {
		return data ;
	}
}
