package parcare.nae;

public abstract class Vehicul {
	
	private String marca , model , type;
	private int inaltime ;

	public Vehicul(String marca, String model, int inaltime) {
		this.marca = marca;
		this.model = model;
		this.inaltime = inaltime;
	}

	public String getMarca() {
		return marca;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getInaltime() {
		return inaltime;
	}

	public void setInaltime(int inaltime) {
		this.inaltime = inaltime;
	}
	
}
