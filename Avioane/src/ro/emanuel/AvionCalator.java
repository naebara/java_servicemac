package ro.emanuel;

public class AvionCalator extends Avion {

	private int maxPassengers;
	public AvionCalator(String planeId, int totalEnginePower) {
		super(planeId, totalEnginePower);
	}
	public AvionCalator(String planeId, int totalEnginePower, int maxPassengers) {
		super(planeId, totalEnginePower);
		this.maxPassengers = maxPassengers;
	}

	public int getMaxPassengers() {
		return maxPassengers;
	}
	public void setMaxPassengers(int maxPassengers) {
		this.maxPassengers = maxPassengers;
	}

}
