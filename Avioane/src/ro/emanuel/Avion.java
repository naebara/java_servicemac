package ro.emanuel;

public class Avion {
	private String planeId;
	private int totalEnginePower;
	
	
	public Avion(String planeId, int totalEnginePower) {
		super();
		this.planeId = planeId;
		this.totalEnginePower = totalEnginePower;
	}
	public void takeOff() {
		System.out.println("PlaneID Value - Initiating takeoff procedure - Starting engines - Accelerating down the runway - Taking off - Retracting gear - Takeoff complete”");
	}
	public void fly() {
		System.out.println("PlaneID Value - Flying");
	}
	public void land() {
		System.out.println("PlaneID Value - Initiating landing procedure - Enabling airbrakes -Lowering gear - Contacting runway - Decelerating - Stopping engines – Landing complete");
	}
	
	public String getPlaneId() {
		return planeId;
	}
	public void setPlaneId(String planeId) {
		this.planeId = planeId;
	}
	
	public int getTotalEnginePower() {
		return totalEnginePower;
	}
	
	public void setTotalEnginePower(int totalEnginePower) {
		this.totalEnginePower = totalEnginePower;
	}
	
	
}

