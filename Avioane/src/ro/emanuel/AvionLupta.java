package ro.emanuel;

public class AvionLupta extends Avion {

	public AvionLupta(String planeId, int totalEnginePower) {
		super(planeId, totalEnginePower);
	}

	public void launchMissle() {
		System.out.println("PlaneID Value - Initiating missile launch procedure - Acquiring target - Launching missile – Breaking away - Missile launch complete");
	}
	
}
