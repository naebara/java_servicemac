package ro.emanuel;

public class Main {

	public static void main(String[] args) {
		
		
		Tara ro = new Tara();
		
		AvionCalator boeing = new BoeingAvion("F56", 7000, 300);
		ro.addAvion(boeing);
		
		AvionLupta a = new AvionLupta("Y45F", 234);
		ro.addAvion(a);
		a.launchMissle();
		
		TomCatAvion tom = new TomCatAvion("T44", 78000); 
		ro.addAvion(tom);
		tom.refuel();
		
		ConcordeAvion c = new ConcordeAvion("ddd", 44);
		ro.addAvion(c);
		c.goSuperSonic();
	
		MigAvion m = new MigAvion("F552", 3452);
		ro.addAvion(m);
		m.highSpeedGeometry();
		m.normalGeometry();
		
	}

}
