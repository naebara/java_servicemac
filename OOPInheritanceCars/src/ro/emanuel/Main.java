package ro.emanuel;

public class Main {

	public static void main(String[] args) {
		
		Car c = new Car();
		c.startEngine();
		c.move();
		c.stopEngine();
		
		Dacia d = new Dacia();
		d.startEngine();
		d.move();
		d.stopEngine();
		
		
		BMW b = new BMW();
		b.startEngine();
		b.move();
		b.stopEngine();
		
	}

}
