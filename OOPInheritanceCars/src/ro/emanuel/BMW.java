package ro.emanuel;

public class BMW extends Car {
	
	public void startEngine() {
		System.out.println("BMW - start engine");
		super.startEngine();
		this.setLightsOn(true);
	}
	public void stopEngine() {
		System.out.println("bmw - stop engine");
		super.stopEngine();
		this.setLightsOn(false);
	}
}
