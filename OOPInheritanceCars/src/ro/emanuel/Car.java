package ro.emanuel;

public class Car {
	private int nOfWheels;
	private String engineType ;
	private boolean isOn ;
	private boolean isLightsOn;
	
	public Car() {
		
	}
	public Car(int nOfWheels, String engineType, boolean isOn, boolean isLightsOn) {
		super();
		this.nOfWheels = nOfWheels;
		this.engineType = engineType;
		this.isOn = isOn;
		this.isLightsOn = isLightsOn;
	}

	public void startEngine() {
		System.out.println("CAR -START ENGINE");
		this.isOn = true;
	}
	
	public void stopEngine() {
		System.out.println("CAR -STOP ENGINE");
		this.isOn = false;
	}
	public void move() {
		System.out.println("CAR - Move");
	}
	public int getnOfWheels() {
		return nOfWheels;
	}

	public void setnOfWheels(int nOfWheels) {
		this.nOfWheels = nOfWheels;
	}

	public String getEngineType() {
		return engineType;
	}

	public void setEngineType(String engineType) {
		this.engineType = engineType;
	}

	public boolean isOn() {
		return isOn;
	}

	public void setOn(boolean isOn) {
		this.isOn = isOn;
	}

	public boolean isLightsOn() {
		return isLightsOn;
	}

	public void setLightsOn(boolean isLightsOn) {
		this.isLightsOn = isLightsOn;
	}
	
	
	
}
