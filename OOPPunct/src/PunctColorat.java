
public class PunctColorat extends Punct {
	private String codCuloare;
	public PunctColorat(int x, int y , String codCuloare) {
		super(x, y);
		this.codCuloare = codCuloare;
	}
	
	public String toString() {
		return 	"acesta este un punct cu colorat cu coordonatele: " + "("+  this.getX() + " , " + this.getY() +
				") si are culoarea: " + this.codCuloare ;
		}
}
