
public class Punct {
	private int x , y ;
	public Punct(int x , int y ) {
		this.x = x ;
		this.y = y ;
	}
	public String toString() {
	return 	"acesta este un punct cu coordonatele : " + this.x + " , " + this.y ;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	
}
