

import java.util.HashMap;
import java.util.Map;

public class HashToHashCopy {

	public static void main(String[] args) {
		
		HashMap<Integer, String> map1 = new HashMap<Integer, String>();
		HashMap<Integer, String> map2 = new HashMap<Integer, String>();
		
		map1.put(1, "Nae");
		map1.put(2, "Sergiu");
		map1.put(3, "Andrei");
		map1.put(4, "Maria");
		map1.put(5, "Emil");
		
		map2.put(6, "Hello");
		
//		for(Integer key : map1.keySet()) {
//			map2.put(key , map1.get(key));
//		}
		
		// sau al doilea mod / a doua varianta 
		
		map2.putAll(map1);
		System.out.println(map1);
		System.out.println(map2);
	}
	

}
