import java.util.HashMap;

public class EmptyValuesMappings {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HashMap<Integer, String> map = new HashMap<Integer, String>();
		map.put(1, "Red");
		map.put(2, "Green");
		map.put(3, "Black");
		map.put(4, "");
		map.put(5, "Purple");
		map.put(6, "");
		System.out.println(map);
		System.out.print("Keys with empty values : ");
		for(Integer a : map.keySet()) {
			if(map.get(a)=="") {
				System.out.print(a + ", ");
			}
		}
	}

}
