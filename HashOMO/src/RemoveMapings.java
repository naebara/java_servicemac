import java.util.HashMap;
import java.util.Map;

public class RemoveMapings {
	public static void main(String[] args) {
		HashMap<Integer, String> map = new HashMap<Integer , String>();
		map.put(1, "Red");
		map.put(2, "Green");
		map.put(3, "Black");
		map.put(4, "Orange");
		map.put(5, "Purple");
		map.put(6, "Yellow");
		System.out.println(map);
		
		for(Map.Entry kv : map.entrySet() ) {
			map.remove(kv.getKey());
		}
		
		System.out.println(map);
	}
}
