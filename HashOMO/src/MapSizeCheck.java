import java.util.HashMap;

public class MapSizeCheck {

	public static void main(String[] args) {
		HashMap<Integer, String> map = new HashMap<Integer, String>();
		map.put(1, "Red");
		map.put(2, "Green");
		map.put(3, "Black");
		System.out.println(map);
	
		System.out.println("Our math size is " + map.size() );

	}

}
