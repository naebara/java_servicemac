import java.util.HashMap;

public class CheckKeyForValueGiven {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HashMap<Integer, String> map = new HashMap<Integer, String>();
		map.put(1, "Red");
		map.put(2, "Green");
		map.put(3, "Black");
		map.put(4, "Orange");
		map.put(5, "Purple");
		map.put(6, "Yellow");
		System.out.println(map);
		
		if(map.containsValue("Green")) {
			System.out.println("Yes - our map contains green");
		}
		else {
			System.out.println("No - our map doesn't contain green");
		}
		map.remove(2);
		System.out.println("Removing key with value green...");
		if(map.containsValue("Green")) {
			System.out.println("Yes - our map contains green");
		}
		else {
			System.out.println("Sorry - our map doesn't contain green");
		}
	}

}
