

import java.util.HashMap;

public class WillItContainKeyHashMap {
	public static void main(String[] args) {
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		map.put("Green", 1);
		map.put("Red", 2);
		map.put("Blue", 3);
		map.put("Orange", 4);
		
		System.out.println("Original map  : " + map);
		
		// test if map contains orange
		
		if(map.containsKey("Orange")) {
			System.out.println("Map contains Orange with value  :" + map.get("Orange"));
		}
		
		// test if map contains yellow
		
		if(map.containsKey("Yellow")==false) {
			System.out.println("Map does not contain Yellow...Sorry");
		}
		else {
			System.out.println("Map contains Yellow");
		}
		map.put("Yellow", 2);
		if(map.containsKey("Yellow")==false) {
			System.out.println("Map does not contain Yellow...Sorry");
		}
		else {
			System.out.println("Map contains Yellow");
		}
		System.out.println(map);
	}
}
