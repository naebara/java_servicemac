import java.util.HashMap;
import java.util.Set;

public class SetViewHashMap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HashMap<Integer, String> map = new HashMap<Integer, String>();
		map.put(1, "Red");
		map.put(2, "Green");
		map.put(3, "Black");
		map.put(4, "Orange");
		map.put(5, "Purple");
		map.put(6, "Yellow");
		System.out.println(map);
		
		Set set = map.keySet();
		System.out.println(set);
	}

}
